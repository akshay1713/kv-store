package main

import "fmt"
import "time"
import "net/rpc"
import "math/rand"

//SwimAccessor is the struct which is accessed by peers via RPC
type SwimAccessor struct {
}

//Get sends the current list of peers to the peer which started the RPC. This is
//accessed by a peer which wants to join an existing swarm of peers
func (swimAccessor *SwimAccessor) Get(address *string, membersList *[]string) error {
	memberAddresses := []string{}
	for i := range swimMembersip.membersList {
		memberAddresses = append(memberAddresses, swimMembersip.membersList[i].Address)
	}
	*membersList = memberAddresses
	swimMembersip.add(*address, false)
	return nil
}

//GetData sends the current data stored in memory to the peer which started the RPC. This is
//accessed by a peer which is new to a swarm and wants to upate itself with the existing data in the swarm.
func (swimAccessor *SwimAccessor) GetData(address *string, data *map[string]string) error {
	*data = store.getAll()
	return nil
}

//Announce is used by a peer new to a swarm to announce itself to all existing peers, so that it
//may be added to the list of every peer.
func (swimAccessor *SwimAccessor) Announce(address *string, ack *bool) error {
	fmt.Println("Announce received from ", *address)
	swimMembersip.add(*address, false)
	*ack = true
	return nil
}

//Ping is used to randomly ping peers in a list at a fixed interval, to check its status
func (swimAccessor *SwimAccessor) Ping(disconnected *[]string, ack *bool) error {
	if len(*disconnected) > 0 {
		fmt.Println("Disconnected addresses are ", disconnected)
		swimMembersip.updateDisconnected(*disconnected)
	}
	*ack = true
	return nil
}

//IndirectPing is used by a peer to ping an unresponsive peer via multiple other peers, to confirm its state.
func (swimAccessor *SwimAccessor) IndirectPing(address *string, ack *bool) error {
	*ack = swimMembersip.indirectPing(*address)
	return nil
}

//SwimMembership is the struct used to maintain membership using the SWIM gossip protocol
type SwimMembership struct {
	membersList       []SwimPeer
	unresponseiveList []SwimPeer
	disconnectedList  []string
	nextIndex         int
	pingActive        bool
	pingUnresponsive  bool
}

//SwimPeer represents a single peer in the peer swarm
type SwimPeer struct {
	Address string
	Client  *rpc.Client
}

func (swimMembership *SwimMembership) initialize() {
	swimMembership.membersList = []SwimPeer{}
	swimMembership.unresponseiveList = []SwimPeer{}
	swimMembership.nextIndex = 0
	swimMembership.pingActive = true
	swimMembership.pingMember()
}

//pingMember pings the next member in the list of peers
func (swimMembersip *SwimMembership) pingMember() {
	if swimMembersip.pingActive == false {
		return
	}
	time.AfterFunc(time.Millisecond*pingInterval, swimMembersip.pingMember)
	if len(swimMembersip.membersList) == 0 {
		//No ping needed if no peers available
		swimMembersip.nextIndex = 0
		return
	}
	if swimMembersip.nextIndex >= len(swimMembersip.membersList) {
		//Start over with the first peer in the list if every member is pinged once.
		//Shuffle the list for the next iteration of pings.
		swimMembersip.nextIndex = 0
		swimMembersip.shuffleMembers()
	}
	index := swimMembersip.nextIndex
	//List of disconnected peers is sent with every ping
	disconnected := swimMembersip.disconnectedList
	ack := false
	fmt.Println("Pinging ", swimMembersip.membersList[index].Address, index)
	err := swimMembersip.membersList[index].Client.Call("SwimAccessor.Ping", &disconnected, &ack)
	if err != nil || !ack {
		fmt.Println("Error while pinging: ", err)
		//Peer is unresponsive
		swimMembersip.handleUnresponsivePeer(index, swimMembersip.membersList[index].Address)
	}
	swimMembersip.nextIndex++
}

//shuffleMembers shuffles the current list of peers
func (swimMembersip *SwimMembership) shuffleMembers() {
	currentMembers := swimMembersip.membersList
	for i := range currentMembers {
		j := rand.Intn(i + 1)
		currentMembers[i], currentMembers[j] = currentMembers[j], currentMembers[i]
	}
	swimMembersip.membersList = currentMembers
	fmt.Println("Shuffled")
}

//updateDisconnected updates the maintained list of disconnected peers with the the given list
func (swimMembersip *SwimMembership) updateDisconnected(disconnectedList []string) {
	for i := range disconnectedList {
		exists := false
		for k := range swimMembersip.disconnectedList {
			if swimMembersip.disconnectedList[k] == disconnectedList[i] {
				exists = true
				break
			}
		}
		if exists {
			continue
		}
		_, index := swimMembersip.getPeerWithAddress(disconnectedList[i], swimMembersip.membersList)
		swimMembersip.membersList = append(swimMembersip.membersList[:index], swimMembersip.membersList[index+1:]...)
		swimMembersip.disconnectedList = append(swimMembersip.disconnectedList, disconnectedList[i])
	}
}

//handleUnresponsivePeer pings an unresponsive peer indirectly via a randomly selected subset of available peers,
//and updates the list of disconnected peers if peer is still unresponsive.
func (swimMembersip *SwimMembership) handleUnresponsivePeer(index int, address string) {
	peer := swimMembersip.membersList[index]
	if peer.Address != address {
		peer, index = swimMembersip.getPeerWithAddress(address, swimMembersip.membersList)
	}
	fmt.Println("Peer with address ", peer.Address, "unresponsive")
	currentList := swimMembersip.membersList
	swimMembersip.membersList = append(currentList[:index], currentList[index+1:]...)
	swimMembersip.unresponseiveList = append(swimMembersip.unresponseiveList, peer)
	swimMembersip.pingUnresponsive = true
	swimMembersip.initiateIndirectPing(address)
}

//initiateIndirectPing selects a number of random peers and initiates an indirect ping to the given address via them all.
func (swimMembersip *SwimMembership) initiateIndirectPing(address string) {
	numPeers := len(swimMembersip.membersList)
	if numPeers == 0 {
		fmt.Println("No active peers left")
		return
	}
	pingPeersLength := int(numPeers / 4)
	peerIndices := make(map[int]bool)
	if pingPeersLength == 0 {
		pingPeersLength = 1
		peerIndices[0] = true
	} else {
		peersNeeded := true
		for peersNeeded && pingPeersLength > 0 {
			randomIndex := rand.Intn(numPeers - 1)
			if _, exists := peerIndices[randomIndex]; !exists {
				peerIndices[randomIndex] = true
				pingPeersLength--
			}
		}
	}
	peerAcks := []bool{}
	for randomIndex := range peerIndices {
		ack := false
		swimMembersip.membersList[randomIndex].Client.Call("SwimAccessor.IndirectPing", &address, &ack)
		//Append acks to a list of acks maintained
		peerAcks = append(peerAcks, ack)
	}
	fmt.Println("Peer Acks are ", peerAcks)
	ackFailed := true
	for i := range peerAcks {
		//Ack is successful even if a single peer connects to the unresponsive peer.
		if peerAcks[i] {
			ackFailed = false
			break
		}
	}
	if ackFailed {
		fmt.Println("Peer ack failed for ", address)
		disconnectedPeer, currentIndex := swimMembersip.getPeerWithAddress(address, swimMembersip.unresponseiveList)
		swimMembersip.unresponseiveList = append(swimMembersip.unresponseiveList[:currentIndex], swimMembersip.unresponseiveList[currentIndex+1:]...)
		swimMembersip.disconnectedList = append(swimMembersip.disconnectedList, disconnectedPeer.Address)
		fmt.Println("After indirect ping\n", swimMembersip.unresponseiveList, "\n", swimMembersip.disconnectedList)
	}
}

//indirectPing is the function which is called by a peer to ping another peer via this one.
func (swimMembersip *SwimMembership) indirectPing(address string) bool {
	_, index := swimMembersip.getPeerWithAddress(address, swimMembersip.membersList)
	if index == -1 {
		fmt.Println("Peer for indirect ping not found, could already be suspected", address)
		return false
	}
	ack := false
	err := swimMembersip.membersList[index].Client.Call("SwimAccessor.Ping", swimMembersip.disconnectedList, &ack)
	if err != nil {
		fmt.Println("Error while indirect ping:", err)
	}
	return ack
}

func (swimMembersip *SwimMembership) getPeerWithAddress(address string, peerList []SwimPeer) (SwimPeer, int) {
	for i := range peerList {
		if peerList[i].Address == address {
			return peerList[i], i
		}
	}
	return SwimPeer{}, -1
}

func (swimMembersip *SwimMembership) add(address string, announce bool) {
	c, err := rpc.Dial("tcp", address)
	if err != nil {
		fmt.Println("Error while connecting to potential swim peer: ", err, address)
		return
	}
	swimPeer := SwimPeer{
		Address: address,
		Client:  c,
	}
	swimMembersip.membersList = append(swimMembersip.membersList, swimPeer)
	fmt.Println("Added ", address, " to list")
	var ack bool
	if announce {
		fmt.Println("Announcing to ", address)
		err := c.Call("SwimAccessor.Announce", &localAddress, &ack)
		if err != nil || !ack {
			fmt.Println("Announce failed: ", err, ack)
		}
	}
}

//set sets the list of peers maintained with the given list
func (swimMembersip *SwimMembership) set(membersList []SwimPeer) {
	swimMembersip.membersList = membersList
}

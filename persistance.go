package main

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

//Persistance is the struct used for persisting data to disk and restoring it when the server starts
type Persistance struct {
	dbName      string
	storeTable  string
	logTable    string
	intervalSet bool
	database    *sql.DB
}

func (persistance *Persistance) initialize() {
	persistance.dbName = dbName
	tableName := localAddress + "_store"
	tableName = strings.Replace(tableName, ".", "_", -1)
	tableName = strings.Replace(tableName, ":", "_", -1)
	persistance.storeTable = tableName
	persistance.logTable = strings.Replace(tableName, "store", "log", 1)
	persistance.createTables()
}

//getExistingStore gets the key value pairs saved on disk
func (persistance *Persistance) getExistingStore() map[string]string {
	existingStore := make(map[string]string)
	query := fmt.Sprintf("SELECT key, value from `%s`", persistance.storeTable)
	statement, err := persistance.database.Prepare(query)
	var key string
	var value string
	if err != nil {
		fmt.Println("Error while checking existing data", err)
		return existingStore
	}
	data, err := statement.Query()
	if err != nil {
		fmt.Println("Error while checking existing data", err)
		return existingStore
	}
	defer data.Close()
	for data.Next() {
		err = data.Scan(&key, &value)
		if err != nil {
			fmt.Println("Error while scanning for values ", err)
			continue
		}
		existingStore[key] = value
	}
	return existingStore
}

//getExistingLog returns the key value data stored in the log, but not yet persisted to the data table
func (persistance *Persistance) getExistingLog() map[string]string {
	existingStore := make(map[string]string)
	query := fmt.Sprintf("SELECT operation, key, value from `%s` order by time asc", persistance.logTable)
	statement, err := persistance.database.Prepare(query)
	var operation string
	var key string
	var value string
	if err != nil {
		fmt.Println("Error while checking existing data", err)
		return existingStore
	}
	data, err := statement.Query()
	if err != nil {
		fmt.Println("Error while checking existing data", err)
		return existingStore
	}
	defer data.Close()
	for data.Next() {
		err = data.Scan(&operation, &key, &value)
		if err != nil {
			fmt.Println("Error while scanning for values ", err)
			continue
		}
		if operation == "set" {
			existingStore[key] = value
		} else {
			delete(existingStore, key)
		}
	}
	return existingStore
}

//commit function stores a key value pair on log, from where it goes to the permanent data table in the future
func (persistance *Persistance) commit(operation string, key string, value string, timestamp uint32) {
	query := fmt.Sprintf("INSERT INTO `%s` values (null, 'set', '%s', '%s', %d)", persistance.logTable, key, value, timestamp)
	statement, err := persistance.database.Prepare(query)
	if err != nil {
		fmt.Println("Error while preparing statement", err)
		return
	}
	statement.Exec()
}

//Dump the given data in the data table
func (persistance *Persistance) dumpData(data map[string]string) {
	query := fmt.Sprintf("DELETE FROM `%s`", persistance.storeTable)
	statement, err := persistance.database.Prepare(query)
	if err != nil {
		fmt.Println("Error while deleting data before inserting ", err)
		return
	}
	statement.Exec()
	for key, value := range data {
		query = fmt.Sprintf("INSERT INTO `%s` values (null, '%s', '%s')", persistance.storeTable, key, value)
		statement, err := persistance.database.Prepare(query)
		if err != nil {
			fmt.Println("Error while inserting data ", key, value, err)
			continue
		}
		statement.Exec()
	}
	//Clear logs table
	query = fmt.Sprintf("DELETE FROM `%s`", persistance.logTable)
	statement, err = persistance.database.Prepare(query)
	if err != nil {
		fmt.Println("Error while deleting data before inserting ", err)
		return
	}
	statement.Exec()
}

//initiateDump periodically dumps the data from memory, into the store table
func (persistance *Persistance) initiateDump() {
	if !persistance.intervalSet {
		return
	}
	data := store.getAll()
	persistance.dumpData(data)
	time.AfterFunc(time.Millisecond*persistInterval, persistance.initiateDump)
}

func (persistance *Persistance) createTables() {
	database, err := sql.Open("sqlite3", persistance.dbName)
	persistance.database = database
	if err != nil {
		fmt.Println("Error while creating DB", err)
	}
	query := fmt.Sprintf("CREATE TABLE IF NOT EXISTS `%s` (id INTEGER PRIMARY KEY, key TEXT, value TEXT)", persistance.storeTable)
	statement, err := database.Prepare(query)
	if err != nil {
		fmt.Println("Error while creating store table ", err)
	}
	statement.Exec()
	query = fmt.Sprintf("CREATE TABLE IF NOT EXISTS `%s` (id INTEGER PRIMARY KEY, operation TEXT, key TEXT, value TEXT, time timestamp)", persistance.logTable)
	statement, err = database.Prepare(query)
	if err != nil {
		fmt.Println("Error while creating log table ", err)
	}
	statement.Exec()
	persistance.intervalSet = true
	time.AfterFunc(time.Millisecond*persistInterval, persistance.initiateDump)
}

//restoreData will get the data stored on disk and set it in memory to be used
func (persistance *Persistance) restoreData() {
	fmt.Println("Restoring data from disk")
	storeData := persistance.getExistingStore()
	logData := persistance.getExistingLog()
	store.setMultiple(storeData)
	store.setMultiple(logData)
}

//clearData will clear data on disk
func (persistance *Persistance) clearData() {
	fmt.Println("Clearing data on disk")
	query := fmt.Sprintf("DELETE FROM `%s`", persistance.storeTable)
	statement, err := persistance.database.Prepare(query)
	if err != nil {
		fmt.Println("Error while clearing store table ", err)
		return
	}
	_, err = statement.Exec()
	if err != nil {
		fmt.Println("Error while clearing store table ", err)
		return
	}
	query = fmt.Sprintf("DELETE FROM `%s`", persistance.logTable)
	statement, err = persistance.database.Prepare(query)
	if err != nil {
		fmt.Println("Error while clearing log table ", err)
		return
	}
	_, err = statement.Exec()
	if err != nil {
		fmt.Println("Error while clearing log table ", err)
	}
}

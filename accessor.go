package main

import (
	"flag"
	"fmt"
	"net"
	"net/rpc"
	"os"
	"strconv"
)

var swimMembersip SwimMembership
var localAddress string
var store Store

func main() {
	rpc.Register(new(StoreAccessor))
	rpc.Register(new(SwimAccessor))
	recvPort := flag.Int("recv", 4000, "port to for receiving messages")
	member := flag.String("member", "", "Address of the member node of existing network. A new network is started if not provided")
	flag.Parse()
	ip := getIP()
	localAddress = ip + ":" + strconv.Itoa(*recvPort)
	swimMembersip.initialize()
	joining := false
	if *member != "" {
		joining = true
	}
	store.initialize(joining)
	if *member != "" {
		joinSwim(*member, *recvPort)
	} else {
		startServer(*recvPort)
	}
}

func joinSwim(address string, recvPort int) {
	fmt.Println("Joining ", address)
	c, err := rpc.Dial("tcp", address)
	if err != nil {
		fmt.Println("Error while joining swim server", err)
		return
	}
	var response []string
	go func() {
		memberErr := c.Call("SwimAccessor.Get", &localAddress, &response)
		if memberErr != nil {
			panic(memberErr)
		}
		for i := range response {
			swimMembersip.add(response[i], true)
		}
		swimMembersip.add(address, false)
		kvData := make(map[string]string)
		memberErr = c.Call("SwimAccessor.GetData", &localAddress, &kvData)
		if memberErr != nil {
			panic(memberErr)
		}
		store.setMultiple(kvData)
		store.persistance.dumpData(kvData)
	}()
	startServer(recvPort)
}

func startServer(recvPort int) {
	fmt.Println("Server started at ", localAddress)
	recvPortString := ":" + strconv.Itoa(recvPort)
	listen, err := net.Listen("tcp", recvPortString)
	if err != nil {
		fmt.Println("Error while starting server ", err)
		return
	}

	defer listen.Close()
	for {
		conn, err := listen.Accept()
		if err != nil {
			fmt.Println("Error: ", err)
			continue
		}
		go rpc.ServeConn(conn)
	}
}

func getIP() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		os.Stderr.WriteString("Oops: " + err.Error() + "\n")
		os.Exit(1)
	}

	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return ""
}

package main

import (
	"errors"
	"math"
	"net/rpc"
	"time"
)

type Args struct {
	Key       string
	Value     string
	Timestamp uint32
}

//StoreAccessor is the struct which is used to access the store by clients via RPC
type StoreAccessor struct {
}

//GetAll returns all the key value data currently in memory
func (this *StoreAccessor) GetAll(args *Args, response *map[string]string) error {
	*response = store.getAll()
	return nil
}

//Get returns a value for the given key
func (this *StoreAccessor) Get(args *Args, reply *string) error {
	value := store.get(args.Key)
	*reply = value
	return nil
}

//Del deletes the given key from the store
func (this *StoreAccessor) Del(args *Args, reply *bool) error {
	peers := swimMembersip.membersList
	args.Timestamp = uint32(time.Now().Unix())
	if len(peers) == 0 {
		//Commit if no other peers available
		store.commitDel(args.Key, args.Timestamp)
		*reply = true
		return nil
	}
	responses := make([]bool, len(peers))
	peerCalls := make([]*rpc.Call, len(peers))
	//Start quorum procedure
	for i := range peers {
		peerCalls[i] = peers[i].Client.Go("StoreAccessor.InitiateDel", args, &responses[i], nil)
	}
	//Initiate delete operation on itself
	responses = append(responses, store.initiateDel(args.Key, args.Timestamp))
	for i := range peerCalls {
		<-peerCalls[i].Done
	}
	// Quorum is calculated as 2/3 of total peers. Make this customizable?
	quorumLength := int(math.Ceil(float64(len(peers)) * 2 / 3))
	for i := range responses {
		if responses[i] {
			quorumLength--
		}
		//break if quorum is reached
		if quorumLength == 0 {
			break
		}
	}
	if quorumLength != 0 {
		*reply = false
		return errors.New("Unable to get quorum")
	}
	//Commit operation for all peers. Doesn't matter if the individual peer approved or not, quorum is reached.
	for i := range peers {
		peerCalls[i] = peers[i].Client.Go("StoreAccessor.CommitDel", args, &responses[i], nil)
	}
	//Commit on itself
	store.commitDel(args.Key, args.Timestamp)
	*reply = true
	return nil
}

//InitiateDel is used as the function for initiating a delete operation, and works by getting a quorum of members to agree
func (this *StoreAccessor) InitiateDel(args *Args, reply *bool) error {
	*reply = store.initiateDel(args.Key, args.Timestamp)
	return nil
}

//CommitDel is used to finalize a delete operation initiated earlier, once quorum is reached
func (this *StoreAccessor) CommitDel(args *Args, reply *bool) error {
	store.commitDel(args.Key, args.Timestamp)
	*reply = true
	return nil
}

//Set sets a given key to the given value in the store
func (this *StoreAccessor) Set(args *Args, reply *bool) error {
	peers := swimMembersip.membersList
	args.Timestamp = uint32(time.Now().Unix())
	if len(peers) == 0 {
		//If no peers available, commit and return
		store.commitSet(args.Key, args.Value, args.Timestamp)
		*reply = true
		return nil
	}
	responses := make([]bool, len(peers))
	recvdResponses := make([]bool, len(peers))
	peerCalls := make([]*rpc.Call, len(peers))
	//Start quorum procedure
	for i := range peers {
		peerCalls[i] = peers[i].Client.Go("StoreAccessor.InitiateSet", args, &responses[i], nil)
	}
	//Initiate a set operation on itself
	recvdResponses = append(recvdResponses, store.initiateSet(args.Key, args.Value, args.Timestamp))
	for i := range peerCalls {
		<-peerCalls[i].Done
		recvdResponses = append(recvdResponses, *peerCalls[i].Reply.(*bool))
	}
	// Another quorum option can be used here
	quorumLength := int(math.Ceil(float64(len(peers)) * 2 / 3))
	for i := range recvdResponses {
		if recvdResponses[i] {
			quorumLength--
		}
		if quorumLength == 0 {
			break
		}
	}
	if quorumLength != 0 {
		*reply = false
		return errors.New("Unable to get quorum")
	}
	//Commit set operation for all peers. Doesn't matter if approved individually by a peer or not, since quorum is reached.
	for i := range peers {
		peerCalls[i] = peers[i].Client.Go("StoreAccessor.CommitSet", args, &responses[i], nil)
	}
	//Commit set operation on itself
	store.commitSet(args.Key, args.Value, args.Timestamp)
	*reply = true
	return nil
}

//InitiateSet starts a set operation on the store. It finishes once a quorum is reached.
func (this *StoreAccessor) InitiateSet(args *Args, reply *bool) error {
	*reply = store.initiateSet(args.Key, args.Value, args.Timestamp)
	return nil
}

//CommitSet commits a set operation started earlier, once quorum is reached
func (this *StoreAccessor) CommitSet(args *Args, reply *bool) error {
	store.commitSet(args.Key, args.Value, args.Timestamp)
	*reply = true
	return nil
}

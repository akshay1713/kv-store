package main

import (
	"bufio"
	"flag"
	"fmt"
	"net/rpc"
	"os"
	"strings"
)

func client() {
	serverAddr := flag.String("addr", "", "Address of K-V store")
	flag.Parse()
	if *serverAddr == "" {
		fmt.Println("Please enter an address for the K-V server using -addr")
		return
	}
	c, err := rpc.Dial("tcp", *serverAddr)
	if err != nil {
		fmt.Println(err)
		return
	}
	startCli(c)
}

func startCli(client *rpc.Client) {
	var userInput string
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Started cli client....")
	for true {
		userInput, _ = reader.ReadString('\n')
		handleCommand(userInput, client)
	}
}

func handleCommand(userInput string, client *rpc.Client) {
	command := strings.TrimSpace(strings.Split(userInput, " ")[0])
	switch command {
	case "set":
		setVal(userInput, client)
	case "get":
		getVal(userInput, client)
	case "del":
		delVal(userInput, client)
	}
}

func setVal(userInput string, client *rpc.Client) {
	parts := strings.Split(userInput, " ")
	key := strings.TrimSpace(parts[1])
	value := strings.TrimSpace(parts[2])
	fmt.Println("Setting ", key, " to ", value)
	var args struct {
		Key   string
		Value string
	}
	args.Key = key
	args.Value = value
	var result bool
	err := client.Call("StoreAccessor.Set", &args, &result)
	if err != nil || result != true {
		fmt.Println("Error while making rpc", err)
		return
	}
}

func getVal(userInput string, client *rpc.Client) {
	parts := strings.Split(userInput, " ")
	var args struct {
		Key string
	}
	if len(parts) == 1 {
		var allResults map[string]string
		err := client.Call("StoreAccessor.GetAll", &args, &allResults)
		if err != nil {
			fmt.Println("Error while making rpc", err)
			return
		}
		for key, value := range allResults {
			fmt.Println(key, " : ", value)
		}
	} else {
		key := strings.TrimSpace(parts[1])
		var result string
		args.Key = key
		err := client.Call("StoreAccessor.Get", &args, &result)
		if err != nil {
			fmt.Println("Error while making rpc", err)
			return
		}
		fmt.Println(result)
	}
}

func delVal(userInput string, client *rpc.Client) {
	parts := strings.Split(userInput, " ")
	key := strings.TrimSpace(parts[1])
	var result bool
	var args struct {
		Key string
	}
	args.Key = key
	err := client.Call("StoreAccessor.Del", &args, &result)
	if err != nil || !result {
		fmt.Println("Error while making rpc", err)
		return
	}
	fmt.Println(key, " deleted")
}

func main() {
	client()
}

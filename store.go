package main

import (
	"sync"
)

type PendingData struct {
	timestamp uint32
	value     string
	operation string
}

type Store struct {
	data        map[string]string
	pendingData map[string]PendingData
	persistance Persistance
	dataLock    sync.Mutex
	pendingLock sync.Mutex
}

func (store *Store) initialize(joining bool) {
	if store.data == nil {
		store.data = make(map[string]string)
	}
	if store.pendingData == nil {
		store.pendingData = make(map[string]PendingData)
	}
	store.persistance.initialize()
	if !joining {
		store.persistance.restoreData()
	} else {
		store.persistance.clearData()
	}
}

func (store *Store) initiateDel(key string, timestamp uint32) bool {
	store.pendingLock.Lock()
	defer store.pendingLock.Unlock()
	currentPending, exists := store.pendingData[key]
	if !exists || currentPending.timestamp < timestamp {
		store.pendingData[key] = PendingData{
			timestamp: timestamp,
			operation: "del",
		}
		return true
	}
	return false
}

func (store *Store) commitDel(key string, timestamp uint32) {
	store.pendingLock.Lock()
	defer store.pendingLock.Unlock()
	store.dataLock.Lock()
	defer store.dataLock.Unlock()
	delete(store.data, key)
	currentPending, exists := store.pendingData[key]
	if exists && currentPending.value == "" {
		delete(store.pendingData, key)
	}
	store.persistance.commit("del", key, "", timestamp)
}

func (store *Store) initiateSet(key string, value string, timestamp uint32) bool {
	store.pendingLock.Lock()
	defer store.pendingLock.Unlock()
	currentPending, exists := store.pendingData[key]
	if !exists || currentPending.timestamp < timestamp {
		store.pendingData[key] = PendingData{
			timestamp: timestamp,
			value:     value,
			operation: "set",
		}
		return true
	}
	return false
}

func (store *Store) setMultiple(data map[string]string) {
	store.dataLock.Lock()
	defer store.dataLock.Unlock()
	for key, value := range data {
		store.data[key] = value
	}
}

func (store *Store) commitSet(key string, value string, timestamp uint32) {
	store.pendingLock.Lock()
	defer store.pendingLock.Unlock()
	store.dataLock.Lock()
	defer store.dataLock.Unlock()
	store.data[key] = value
	currentPending, exists := store.pendingData[key]
	if exists && currentPending.value == value {
		delete(store.pendingData, key)
	}
	store.persistance.commit("set", key, value, timestamp)
}

func (store *Store) get(key string) string {
	store.dataLock.Lock()
	defer store.dataLock.Unlock()
	return store.data[key]
}

func (store *Store) getAll() map[string]string {
	store.dataLock.Lock()
	defer store.dataLock.Unlock()
	return store.data
}
